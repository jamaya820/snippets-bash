#!/bin/bash
# The main purpose of this script is to rotate the event logs in a certain directory
# This script requires 2 parameters : 1 to determine the frequency of log and 2 for the fallback directory
# Sample usage : bash log_rotation.sh h

# Accepts a parameter : h=hourly, d=daily
log_type=$1

# Accepts second parameter which is Fallback directory
#if [ -z "$2" ]; then
#    fallback_dir="$2";
#else
#    fallback_dir="/var/log/frogos/spool_fallback/";
#fi

fallback_dir="/var/log/fallback/";

if [ -d "$fallback_dir" ]; then
    case "$log_type" in
    "h")
            # Get all files in the directory that are created at least an hour before
        # Concatenate them in an hourly log
        # Remove the event logs after concatenating them

        new_log="hourly_$(date +"%H").log"
            array=(`find "$fallback_dir" -mmin -60 ! \( -name "daily*" -o -name "hourly*" \)`)

            for filename in "${array[@]}"
            do :
            if cat "$filename" >> "$fallback_dir""$new_log"; then
            echo "$filename has been appended to $new_log"
            rm -f "$filename"
        else
            echo "ERROR : Failed to append $filename to $new_log"
        fi
        done
            ;;
    "d")
        # Get all files with hourly_* as filename, concatenate them in a daily file
        # Remove the hourly logs

        new_log="daily_$(date +"%m-%d-%y").log"
        array=(`find "$fallback_dir" -name 'hourly_*'`)

        for filename in "${array[@]}"
            do :
                if cat "$filename" >> "$fallback_dir""$new_log"; then
            echo "$filename has been appended to $new_log"
            rm -f "$filename"
        else
            echo "ERROR : Failed to append $filename to $new_log"
        fi
            done        
            ;;
    *)
            echo "No parameter found"  
            ;;
    esac
else
    echo "Directory not found"

fi

